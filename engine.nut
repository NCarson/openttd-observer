
class OBEngine {

	static function GetEngines () { //TODO check IsRailTypeAvailable would fix this nonsense
		local a = []
		local out = []
		local powered = []
		local rt = Set()
		foreach (key, _ in GSEngineList(GSVehicle.VT_RAIL)) {
			if (!GSEngine.IsValidEngine(key))
				continue
			// wagons will still be buildable even if the engine rail type does not exist yet
			if (!GSEngine.IsBuildable(key)) 
				continue
			a.push(key)
			if (!GSEngine.IsWagon(key))
				powered.push(key)
		}
		foreach (engine in powered) { 
			rt.add(GSEngine.GetRailType(engine))
		}
		local rt = OBEngine.GetRailTypes(powered)
		// see if we have a engine to pull the wagon with right rt
		foreach (vehicle in a) {
			if (rt.contains(GSEngine.GetRailType(vehicle)))
				out.append(vehicle)
		}
		return out
	}

	static function IterEngines(keys, railtype) {
		if (!GSRail.IsRailTypeAvailable(railtype))
			return null
		foreach (key in keys) {
			if (!GSEngine.IsValidEngine(key))
				continue
			if (GSEngine.IsWagon(key))
				continue
			if (!GSEngine.GetRailType(railtype) == railtype)
				continue
			yield key
		}
	}

	static function IterCanHaul(keys, cargo) {
		if (!GSCargo.IsValidCargo(cargo))
			return null
		foreach (key in keys) {
			if (!GSEngine.IsValidEngine(key))
				continue
			if (GSEngine.GetCargoType(key) == cargo)
				yield key
		}
	}

	static function GetRailTypes(keys) {
		local rt = Set()
		foreach (k in keys) { 
			rt.add(GSEngine.GetRailType(k))
		}
		return rt
	}

	key = null
	name = null
	rail_type = null
	max_speed = null
	price = null
	running_cost = null
	horsepower = null
	tractive_effort = null // kN
	cargo_key = null
	weight = null // vehicle tons
	capacity = null // cargo tons
	cargo_weight = null
	max_age = null // days
	//TODO reliability 
	
	constructor(key) {
		this.key = key
		this.name = GSEngine.GetName(key)
		local rt = GSEngine.GetRailType(key)
		this.rail_type = GSRail.GetName(rt)
		this.max_speed = GSEngine.GetMaxSpeed(key)
		this.price = GSEngine.GetPrice(key)
		this.running_cost = GSEngine.GetRunningCost(key)
		this.horsepower = GSEngine.GetPower(key)
		this.tractive_effort = GSEngine.GetMaxTractiveEffort(key)
		this.cargo_key = GSEngine.GetCargoType(key)
		this.capacity = GSEngine.GetCapacity(key)
		this.weight = GSEngine.GetWeight(key)
		this.cargo_weight = GSCargo.GetWeight(this.cargo_key, this.capacity)
		this.max_age = GSEngine.GetMaxAge(key)

		if (this.capacity == -1)
			this.capacity = 0
		if (this.cargo_weight == -1)
			this.cargo_weight = 0

		if (this.horsepower == -1)
			this.horsepower = null
		if (this.tractive_effort == -1)
			this.tractive_effort = null
		if (this.max_age == -1)
			this.max_age = null
	}



	static function GetTileLength(key) {
	//XXX make sure we dont call this until the event loop. (segfaults in Init)
	// BUG I cannot believe that you have to build a vehicle to get the length
		local company = GSCompanyMode(0)
		//local mode = GSTestMode() // XXX gives invalid vehicles in test mode
		local n = 0	
		local tile, depot
		GSRail.SetCurrentRailType(0) // make sure railtype is set before building depot
		while (!depot) {
			n++
			if (n>500) {
				GSLog.Warning("Could not find buildable tile for OBEngine.GetLength")
				return null
			}
			tile = OBMap.PickRandomTile()
			if (!GSTile.IsBuildable(tile))
				continue
			local x = GSMap.GetTileX(tile)
			local y = GSMap.GetTileY(tile)
			depot = GSRail.BuildRailDepot(tile, GSMap.GetTileIndex(x-1,y))
		}

		local cargo = GSEngine.GetCargoType(key)
		local vehicle = GSVehicle.BuildVehicleWithRefit(tile, key, cargo)

		//local vehicle = GSVehicle.BuildVehicle(tile, key)// BUG: the C++ code is booby trapped!
		//print(GSVehicle.IsValidVehicle(vehicle)) // this gives null pointer with BuildVehicle

		print(GSVehicle.GetLength(vehicle) / 16.0) // length in 16 parts per tile

		if (!GSVehicle.IsValidVehicle(vehicle)) {
			GSLog.Warning("vehicle is not valid for for OBEngine.GetLength")
			return null
		}
		//print(GSCompanyMode.IsValid()) // BUG not in openttd-13
		local length = GSVehicle.GetLength(vehicle) / 16.0 // length in 16 parts per tile
		GSVehicle.SellVehicle(vehicle)
		return length
	}
}
