class OBLogger {

	_klass = null


	function constructor(klass) {
		this._klass = klass
	}

	function _date() {
		local current_date = GSDate.GetCurrentDate();
		local year = GSDate.GetYear(current_date);
		local month = GSDate.GetMonth(current_date);
		local day = GSDate.GetDayOfMonth(current_date);
		local date = year + "-" + month + "-" + day
		return date
	}
	function _fmt(method, msg) {

		local seconds = GSDate.GetSystemTime()

		return (
			seconds + ": " 
			+ this._date() + ": " 
			+ this._klass + "." + method + ": " 
			+ msg
		)
	}
	//XXX Info does not show up in the debug terminal
	function Info(method, msg)  GSLog.Info(this._fmt(method, msg))
	function Warn(method, msg)  GSLog.Warning(this._fmt(method, msg))
	function Error(method, msg) GSLog.Error(this._fmt(method, msg))
	function Json(json) { // XXX dont use in json | needed for tokens
		print("[J] | " + this._date() + " | " + json)
	}
}
