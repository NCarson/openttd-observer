/**
	Callback class to estimate potential rail lines.

	Fairly naive implementation. Also infrastructure costs are
	just rough guesses. See doc string.
*/
class OBRailCostEstimator {
	
	overruns = 1.25 //fudge factor
	sell_track = 70 //FIXME find this out like train length
	signal_space = 5
	depot_space = 15

	railtype = null
	train_length = null
	track_width = null
	distance = null

	
	/** GSRail.RailType, tiles, number of parallel tracks, Manhattan distance between stations */
	constructor(railtype, train_length, track_width, distance) {
		this.railtype = railtype
		this.train_length = train_length
		this.track_width = track_width
		this.distance = distance
	}

	function _piece_list() {
		local R = GSRail
		local out = []
		local stations = this.train_length * this.track_width
		local rails = this.distance * this.track_width - stations
		out.append([R.BT_STATION, stations])
		out.append([R.BT_TRACK, rails])
		out.append([R.BT_SIGNAL, floor(rails / this.signal_space)])
		out.append([R.BT_DEPOT, floor(rails / this.depot_space)])
		return out
	}

	function SellCapital() {
		// XXX it could be better to take the hit on demolishing everything
		// if infrastructure costs were on
		local sum
		foreach (val in this._piece_list()) {
			if (val[0] == GSRail.BT_TRACK) // only tracks produce money demolishing
				sum = this.sell_track * val[1]
		}
		return sum * this.overruns
	}

	/** Returns an esitmate of the amount of money to build a line. */
	function GetCapitalCost() {
		if (!GSRail.IsRailTypeAvailable(this.railtype))
			return null
		local sum = 0
		foreach (val in this._piece_list()) {
			sum += GSRail.GetBuildCost(this.railtype, val[0]) * val[1]
		}
		return sum * this.overruns
	}

	/** Estimate the new infrastructure costs of building a line.

		BUG: This is really just a wild guess as there is no real 
		way of knowing key multipliers. The game does not give any runtime access
		to signals or rail station costs. We do know that infrastructure
		maintenance works in n*sqrt(n) where n is the amount of rail pieces.
		As an exponential equation, eventually costs will explode. If we see
		that on in the settings, an AI would be well advised to build lightly.
	*/
	function GetInfrastructureCost() {
		if (!GSRail.IsRailTypeAvailable(this.railtype))
			return null
		local on = GSGameSettings.GetValue("economy.infrastructure_maintenance")
		if (!on)
			return 0
		local R = GSRail
		local cost = R.GetMaintenanceCostFactor(this.railtype)
		local n = 0
		foreach (val in this._piece_list()) {
			// signals and stations are not included in "rail pieces"
			// if (val[0] != R.BT_STATION && val[0] != R.BT_SIGNAL) 
			// Oh well, just add it all up. See doc string.
				n += val[1]
		}
		return Math.round(n * cost * this.overruns)
	}
}
