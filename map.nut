class OBMap {
	
	_logger = OBLogger("OBMap")

	static function ToTable() {
		return {
			x=GSMap.GetMapSizeX()
			y=GSMap.GetMapSizeY()
			is_water = OBMap.MapFunctorToString(GSTile.IsWaterTile)
		}
	}

	static function PickRandomTile() {
		local w = GSMap.GetMapSizeX()
		local h = GSMap.GetMapSizeY()
		local x = floor(CoreUtils.Rand.random()*w).tointeger()
		local y = floor(CoreUtils.Rand.random()*h).tointeger()
		local tile = GSMap.GetTileIndex(x, y)
		return tile
	}

	static function IterTiles() {
		for (local x=0; x<GSMap.GetMapSizeX(); x++)
			for (local y=0; y<GSMap.GetMapSizeY(); y++) {
				local tile = GSMap.GetTileIndex(x+1, y+1)
				yield tile
			}
	}
	static function MapFunctorToString(func) {

		local out = []
		local i = 0
		local bits
		local nulls = 0
		local n = 0

		local flipBits = function(n, k) { // for testing
			local mask = 1;
			for (local i = 1; i < k; ++i)
				mask = mask | mask << 1;
			return ~n & mask;
		}

		foreach (tile in OBMap.IterTiles()) {
			n += 1
			if (!GSMap.IsValidTile(tile)) { //print("NULL") //BUG FIXME: the last two y rows independent of map size are invalid!
				//return null
				nulls++
			}
			if (i == 0)
				bits = 0
			local result = func(tile)
			if (result)
				bits =  bits | 1 << i
			i++
			if (i == _intsize_ * 8 / 2) { // 32 bit usually
				i = 0
				//out.append(flipBits(bits, 32))
				out.append(bits)
			}
		}

		if (nulls) {
			local msg = nulls + " invalid tiles found itering map tiles"
			OBMap._logger.Warn("MapFunctorEncodeString", msg)
		}
		local a = []
		foreach (o in out)
			a.append(OBUtil.int2hex(o, true))
		return OBUtil.encode_bits(a)
	}	
}
