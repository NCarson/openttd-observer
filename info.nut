FMainClass <- class extends GSInfo {
	function GetAuthor()		{ return "NCarson"; }
	function GetName()			{ return "Game Observer"; }
	function GetDescription() 	{ return "Gain insight to vehicle profit and performance"; }
	function GetVersion()		{ return 1; }
	function GetDate()			{ return "2023-04-28"; }
	function CreateInstance()	{ return "MainClass"; }
	function GetShortName()		{ return "OBSV"; }
	function GetAPIVersion()	{ return "1.3"; }
	//function GetURL()			{ return ""; }

	function GetSettings() {
		AddSetting({name = "log_level", description = "Debug: Log level (higher = print more)", easy_value = 3, medium_value = 3, hard_value = 3, custom_value = 3, flags = CONFIG_INGAME, min_value = 1, max_value = 3});
		AddLabels("log_level", {_1 = "1: Info", _2 = "2: Verbose", _3 = "3: Debug" } );
	}
}

RegisterGS(FMainClass());
