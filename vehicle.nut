
class OBVehicle { 
	//TODO groups

	//TODO maybe we should put isValidVehicles guards on every function?
	// we will need to because we will have to keep track of profit

	static function GetVehicles(engines) {
		local l = []
		foreach (key, _ in GSVehicleList()) {
			if (!GSVehicle.IsValidVehicle(key))
				continue
			l.push(OBVehicle(key, engines))
		}
		return l
	}

	name = null
	key = null
	number = null

	constructor(key, engines) {

		this.key = key
		this.name = GSVehicle.GetName(key)
		this.number = GSVehicle.GetUnitNumber(key)

		print("")
		print(this.name)
		print(this.number)
		local ckeys = this.GetCargoKeys()
		pprint(this.GetCargoLoad())
		pprint(this.GetCargoCapacity())
		print(this.GetSpeed())
		print(this.GetMph())
		print(this.GetKmh())
		print(this.GetStateString())
		print(this.GetTotalYearlyCost())
		print(this.GetProfitThisYear())
	}

	function GenerateEngineKeys() {

		for (local i=0; i < GSVehicle.GetNumWagons(this.key); i++) {
			local ekey = GSVehicle.GetWagonEngineType(this.key, i)
			if (!GSEngine.IsValidEngine(ekey))
				continue // XXX maybe we should return null?
			yield ekey
		}
		return null
	}

	function GetCargoKeys() {
		local cargo_keys = CoreUtils.Set()
		foreach (ekey in this.GenerateEngineKeys()) {
			local ckey = GSEngine.GetCargoType(ekey)
			if (GSCargo.IsValidCargo(ckey))
				cargo_keys.add(ckey)
		}
		return cargo_keys
	}

	function GetCargoLoad() {
		local t = {}
		foreach(ckey in this.GetCargoKeys()) {
			t[ckey] <- GSVehicle.GetCargoLoad(this.key, ckey)
		}
		return t
	}

	function GetCargoCapacity() {
		local t = {}
		foreach(ekey in this.GenerateEngineKeys()) {
			local ckey = GSEngine.GetCargoType(ekey)
			if (!GSCargo.IsValidCargo(ckey))
				continue
			local capacity = GSEngine.GetCapacity(ekey)
			if (ckey in t)
				t[ckey] += capacity
			else
				t[ckey] <- 0
		}
		return t
	}

	function GetSpeed() {

		/* from the docs:
		The speed is in OpenTTD's internal speed unit. 
		This is mph / 1.6, which is roughly km/h. 
		To get km/h multiply this number by 1.00584.
		*/
		return GSVehicle.GetCurrentSpeed(this.key)
	}

	function GetMph() { // XXX do we know if this might return null? FIXME
		return this.GetSpeed() / 1.6
	}

	function GetKmh() { //FIXME use the util version now
		return 1.00584 * this.GetSpeed()
	}

	function GetStateString() {//TODO this should use lang strings
		local state
		switch (GSVehicle.GetState(this.key)) {
			case GSVehicle.VS_RUNNING:
				state = "running";
				break
			case GSVehicle.VS_STOPPED:
				state = "stopped"
				break
			case GSVehicle.VS_IN_DEPOT:
				state = "in depot";
				break
			case GSVehicle.VS_AT_STATION:
				state = "at station";
				break
			case GSVehicle.VS_BROKEN:
				state = "broken down";
				break
			case GSVehicle.VS_CRASHED:
				state = "crashed";
				break
			case GSVehicle.VS_INVALID:
				state = "invalid";
				break
		}
		return state
	}

	function GetTotalYearlyCost() {
		// includes all the wagons
		return GSVehicle.GetRunningCost(this.key)
	}

	function GetProfitThisYear() {
		return GSVehicle.GetProfitThisYear(this.key)
	}
}


