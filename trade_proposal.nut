class OBTradeProposal {
	
	producer = null
	consumer = null
	cargo = null
	production = null
	distance = null

	constructor(producer, consumer, cargo) {

		this.producer = producer
		this.consumer = consumer
		this.cargo = cargo

		local tile = this.consumer.GetTile()
		this.distance = GSIndustry.GetDistanceManhattanToTile(this.producer.key, tile)
		this.production = GSIndustry.GetLastMonthProduction(this.producer.key, cargo)
	}

	function tostring() {
		local cargo = GSCargo.GetName(cargo)
		local p = this.producer.GetName()
		local c = this.consumer.GetName()
		return (this.production + " " 
			+ cargo + " | " + p 
			+ " -> (" + this.distance + ") " 
			+ c
			)
	}

	function GetTradeRouteTrains(railtype, train_length, cost_estimator=null) {

		local out = []
		local production = GSIndustry.GetLastMonthProduction(this.producer.key, this.cargo)
		local engines = OBEngine.GetEngines()
		foreach (key in OBEngine.IterEngines(engines, railtype)) {
			local engine = OBTradeRouteEngine(
				key, 
				this.cargo, 
				this.distance, 
				train_length
				)
			foreach (wagon in engine.IterWagons(engines, train_length)) {
				local train = OBTradeRouteTrain(
					engine, 
					wagon, 
					production*12, 
					cost_estimator
					)
				out.push(train)
			}
		}
		return out
	}
}
