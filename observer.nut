/** Top level user level code class.
	To be seperated away from lib code.
*/

	/*
		IsBuildable // true for clear, trees, or coast tiles (which are also water)
		IsSteepSlope, IsHalftilesSlope
		Is ... Tile
		sea, river, water, coast
		station,
		has tree on tile
		is farm tile
		is rock tile
		is rough tile
		is snow tile
		is desert tile

		get slope
		get min height
		get max height
		get owner
		has transport type

		is within town influence
		looks like to figure houses you have to use: getbuildcost and look for high prices
	*/


class OBObserver {

	_logger = OBLogger("OBbserver")

	
	constructor() {
		//OBVehicle.GetVehicles(this.engines)
		//this.engines = OBEngine.GetEngines()
		//this.cargos = OBCargo.GetCargos()
		//print(OBEngine.GetTileLength(this.engines[0]))
		//this.industries = 
		//local story = OBStoryPage()
		//GetIndustryTrades()
		this.ToJson()
	}

	function ToJson() {
		//local water = OBMap.MapFunctorJson("is_water", GSTile.IsWaterTile)
		local t = {
			map = OBMap.ToTable()
			towns = OBTown.ToArray()
			industries = OBIndustry.ToArray()
		}
		this._logger.Json(Json.To(t))
	}

	function GetIndustryTrades() {

		local cargos = OBCargo.GetCargos()
		local industries = OBIndustry.GetIndustries()
		local railtype = 0 //FIXME
		local train_size = 3 //FIXME
		local track_width = 2 //FIXME

		foreach (cargo in cargos) {
			local trades = OBIndustry.GetIndustryTrades(industries, cargo)
			foreach (key, vals in trades) {
				local distance = function(a, b) {
					if (a.distance > b.distance) return -1
					if (a.distance < b.distance) return 1
					return 0
				}
				vals.sort(distance) // we will pick the shortest ones
				local best = vals[0]
				print("")
				print(best.tostring())
				local estimator = OBRailCostEstimator(
					railtype, 
					train_size, 
					track_width, 
					best.distance
					)

				local trains = []
				foreach (size in [4,5,6])
					trains.extend(best.GetTradeRouteTrains(railtype, size, estimator))
				local train = Array.max(trains) // highest roi
				//foreach (train in trains) {
					print("")
					print(train.tostring())
					print(train.engine.tostring())
					print(train.wagons.tostring())
					print("capital  " + (train.GetCapitalCost() / 1000).tointeger())
					print("roi1     " + train.GetROI())
					print("roi5     " + train.GetROI(5))
					/*
					print("vehicle  " + train.GetVehiclesCost())
					print("dvehicle " + train.DevalueVehicles(1))
					print("profit   " + train.GetProfit())
					print("sell     " + train.SellCapital())
					print("build    " + train.GetBuildCost())
					print("maint    " + (train.GetInfrastructureCost() / 1000).tointeger())
					*/
				//}
			}
		}
	}
}
