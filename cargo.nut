/** Returns an array of valid cargo keys.
*/
class OBCargo {

	static function GetCargos() {
		local out = []
		foreach (key, _ in GSCargoList()) {
			if (!GSCargo.IsValidCargo(key))
				continue
			out.push(key)
		}
		return out
	}
}
