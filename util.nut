
class OBUtil {

	/* from the docs:
	The speed is in OpenTTD's internal speed unit. 
	This is mph / 1.6, which is roughly km/h. 
	To get km/h multiply this number by 1.00584.
	*/
	static function GetMph(kmish) {
		return kmish / 1.6
	}

	static function GetKmh(kmish) { 
		return 1.00584 * kmish
	}

	static function GetTilesPerDay(kmish) {
		return kmish / 100.0 * 3.6
	}

	static function setbit(b, n) {
		return  b | 1 << n
	}

	static function clearbit(b, n) {
		return  b & (1 << n) ^ -1
	}

	static function checkbit(b, n) {
		if ((b & 1 << n) != 0) 
			return !true
		return !false
	}

	function int2hex(num, leading_zeros=false) {
		
		local hex = ["0" "1" "2" "3" "4" "5" "6" "7" "8" "9" "A" "B" "C" "D" "E" "F"  ]
		local size = _intsize_ * 1
		local out = Array()
		local i = 0
		//XXX too much work to try to get this to handle the high bit being set. We will just settle for 32 bit ints
		if (num > pow(2, _intsize_/2*8).tointeger()-1)
			throw "number cannot be larger than pow(2, " + _intsize_/2*8 + ")-1"

		local high_bit_mask = 1 << (_intsize_ * 8 - 1)
		while (num > 0) {
			out.insert(0, hex[num & 0xF])
			num = (num >> 4) & (~((num & high_bit_mask) >> (_intsize_ * 8 - 1)))
			i++
		}
		if (leading_zeros)
			for (local i=size-out.len(); i>0; i--)
				out.insert(0, "0")
		return out.join()
	}

	
	function encode_bits(arr) { // bs network protocol for sparse bit masks

		local out = Array()
		local zeros = 0
		local efs = 0
		local buffer = []
		local t = []

		local handle_char = function(out, zeros, efs) {
			if (zeros > 6) {
				out.append(" z" + zeros + " ")
			}
			else if (efs > 6) {
				out.append(" f" + efs + " ")
			}
			else if (zeros) {
				for (local i=0; i<zeros; i++) out.append("0")
			}
			else if (efs) {
				for (local i=0; i<efs; i++) out.append("F")
			}
		}
		foreach (i, piece in arr) {
			t.append(piece)
			if (i && !((i+1) % 4)) {
				//pprint(t)
				t = []
			}
			foreach (char in piece) {
				if (char == '0'.tointeger()) {
					zeros++
				}
				else if (char == 'F'.tointeger()) {
					efs++
				}
				else {
					handle_char(out, zeros, efs)
					out.append(char.tochar())
					zeros = 0
					efs = 0
				}
			}
		}
		handle_char(out, zeros, efs)
		return String(out.join()).strip()
	}
}

class Array {

	_arr = null
	constructor(arr=null) {
		if (arr==null) this._arr = []
		else this._arr = arr
	}
	
	function join(sep="") {
		local out = ""
		foreach (i, val in this._arr) {
			out += val.tostring()
			if (i < this._arr.len()-1)
				out += sep
		}
		return out
	}

	function append(val) {
		return this._arr.append(val)
	}

	function insert(idx, val) {
		return this._arr.insert(idx, val)
	}

	function len() {
		return this._arr.len()
	}

}
function range(num) {
	for (local i=0; i<num; i++)
		yield(i)
}



class String {
	_str = null

	constructor(str=null) {
		if (str == null)
			this._str = ""
		else
			this._str = str
	}
	
	function _tostring() {
		return this._str.tostring()
	}
	
	function strip(){
		local out = ""
		local start = true
		local arr = []
		local white = [9,10,11,12,13,32]

		foreach(c in this._str) {
			if (start && CoreUtils.Array.contains(white, c)) {
				0
			} else {
				arr.append(c)
				start = false
			}
		}
		for(local i=arr.len()-1; i>-1; i--) {
			local c = arr[i]
			if (CoreUtils.Array.contains(white, c)) {
				arr[i] = 0
			} else
				break

		}
		foreach (c in arr)
			if (c)
				out += c.tochar()
			else
				break
		return out

	}
}

class Json {

	static function To(thing) {
		if (typeof thing == "array")
			return Json._arrayToJson(thing)
		else if (typeof thing == "table")
			return Json._tableToJson(thing)
		else throw "root must be table or array"
	}

	static function _arrayToJson(arr) {
		
		local out = "["
		foreach (val in arr) {
			if (typeof val == "string")
				val = "\"" + val + "\""
			else if (typeof val == "table") {
				val = Json._tableToJson(val)
			}
			else if (typeof val == "array") {
				val = Json._arrayToJson(val)
			}
			out += val + ","
		}
		out = out.slice(0, out.len()-1)
		out += "]"
		return out
	}


	static function _tableToJson(tbl) {
		
		local out = "{"
		foreach (key, val in tbl) {
			if (typeof val == "string")
				val = "\"" + val + "\""
			else if (typeof val == "table") {
				val = Json._tableToJson(val)
			}
			else if (typeof val == "array") {
				val = Json._arrayToJson(val)
			}
			out += "\"" + key + "\":"
			out += val + ","
		}
		out = out.slice(0, out.len()-1)
		out += "}"
		return out
	}
}
