GITLAB_USER = NCarson
GITLAB_PROJECT = openttd-observer

INSTALL_DIR = ~/.local/share/openttd/game/
SRC = $(wildcard *.nut ) lang readme.txt license.txt #changelog.txt

NAME = $(shell grep GetName info.nut | cut -d'"' -f 2 | tr ' ' _)
DIR_NAME = $(NAME)
TAR = $(DIR_NAME).tar

#########################
# big phonies
#

.PHONY: all
all : $(TAR)

.PHONY: clean
clean:
	rm -fr $(DIR_NAME) $(TAR)

.PHONY: install
install:
	cp -a $(TAR) $(INSTALL_DIR)

.PHONY: uninstall
uninstall:
	rm -f $(INSTALL_DIR)$(TAR)

.PHONY: fixme
fixme: clean #FIXME should depend on sources
	grep -rn \
		--exclude=Makefile\
		--exclude=.Makefile.swp\
		--exclude=FIXME.txt\
		FIXME .\
		| sed G\
		> FIXME.txt

#########################
# git
#


#derived
GIT_LAB_SSH = git@gitlab.coam:$(USER)/$(PROJECT).git

.PHONY: push-new
push-new:
	git remote add origin $(GIT_LAB_SSH)
	git push -u origin --all
	git push -u origin --tags

.PHONY: push
push: anoncommit
	git push origin master

.PHONY: anoncommit
anoncommit: fixme
	git add . &&  git commit -m ...

#########################
# source
#

$(TAR): $(SRC)
	mkdir -p $(DIR_NAME)
	cp -Ra $(SRC) $(DIR_NAME)
	tar -cf  $(TAR) $(DIR_NAME)

