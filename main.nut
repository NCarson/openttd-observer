
dofile <- require 
import("util.CoreUtils", "CoreUtils", 10)
pprint <- CoreUtils.pprint
Array <- CoreUtils.Array
Set <- CoreUtils.Set
Table <- CoreUtils.Table
Rand <- CoreUtils.Rand
Math <- CoreUtils.Math

/** Import SuperLib for GameScript **/
import("util.superlib", "SuperLib", 36);
Log <- SuperLib.Log;
Helper <- SuperLib.Helper;

// Remember to set dependencies in the bananas web manager for all libraries
// that you use. This way users will automatically get all libraries of the
// version you selected when they download your Game Script.

/** Import other source code files **/
require("logger.nut") //XXX make sure this first as the loggers are in the class defs!
require("observer.nut")

require("story.nut")
require("map.nut")
require("cargo.nut")
require("engine.nut")
require("vehicle.nut")
require("industry.nut")
require("town.nut")

require("util.nut")
require("trade_proposal.nut")
require("base_trade_route.nut")
require("trade_route_train.nut")
require("rail_cost_estimator.nut")

require("test.nut")


class MainClass extends GSController 
{
	// how often we should poll in the main event loop
	sleep_ticks = 74 // 1 day

	_loaded_data = null
	_loaded_from_version = null
	_init_done = null
	_first_tick = null

	observer = null // our main entry point

	/*
	 * This method is called when your GS is constructed.
	 * It is recommended to only do basic initialization of member variables
	 * here.
	 * Many API functions are unavailable from the constructor. Instead do
	 * or call most of your initialization code from MainClass::Init.
	 */
	constructor()
	{
		this._init_done = false
		this._loaded_data = null
		this._loaded_from_version = null
		this._first_tick = true
	}

	function AfterInit() { }
	function EndOfYear() {}
	function EndOfMonth() {}
	function EndOfDay() { 
	}
	// every time we poll. see sleep_ticks
	function EndOfTicks() { // first tick still not everything has settled down
		if (this._first_tick) {
			this.observer = OBObserver() // we have to do this here to make sure world is generated
			this._first_tick = false
		}
	}

// Begin the event loop
function Start()
{
	// Some OpenTTD versions are affected by a bug where all API methods
	// that create things in the game world during world generation will
	// return object id 0 even if the created object has a different ID. 
	// In that case, the easiest workaround is to delay Init until the 
	// game has started.
	if (Helper.HasWorldGenBug()) GSController.Sleep(1);
	this.Init();
	// Wait for the game to start (or more correctly, tell OpenTTD to not
	// execute our GS further in world generation)
	GSController.Sleep(1);
	// Game has now started and if it is a single player game,
	// company 0 exist and is the human company.

	// Main Game Script loop
	local last_loop_date = GSDate.GetCurrentDate();
	local first = true
	while (true) {
		local loop_start_tick = GSController.GetTick();
		// Handle incoming messages from OpenTTD
		this.HandleEvents();
		// Reached new year/month?
		local current_date = GSDate.GetCurrentDate();
		if (last_loop_date != null) {

			local year = GSDate.GetYear(current_date);
			local month = GSDate.GetMonth(current_date);
			local day = GSDate.GetDayOfMonth(current_date);

			if (year != GSDate.GetYear(last_loop_date)) {
				this.EndOfYear();
			}
			if (month != GSDate.GetMonth(last_loop_date)) {
				this.EndOfMonth();
			}
			if (day != GSDate.GetDayOfMonth(last_loop_date)) {
				this.EndOfDay();
			}
			if (last_loop_date != current_date || first) { // if we are not paused
				this.EndOfTicks()
			}
			first = false
		}
		last_loop_date = current_date;
		// Loop with a frequency of five days
		local ticks_used = GSController.GetTick() - loop_start_tick;
		GSController.Sleep(max(1, this.sleep_ticks - ticks_used));
	}
}

function HandleEvents()
{
	if(GSEventController.IsEventWaiting()) {
		local ev = GSEventController.GetNextEvent();
		if (ev == null) return;
		local ev_type = ev.GetEventType();
		switch (ev_type) {
			case GSEvent.ET_COMPANY_NEW: {
				local company_event = GSEventCompanyNew.Convert(ev);
				local company_id = company_event.GetCompanyID();
				// Here you can welcome the new company
				//Story.ShowMessage(company_id, GSText(GSText.STR_WELCOME, company_id));
				break;
			}
			// other events ...
		}
	}
}

// this called on construction and when the game is reloaded
function Init()
{
	if (this._loaded_data != null) {
		// Copy loaded data from this._loaded_data to this.*
		// or do whatever you like with the loaded data
	} else {
		// construct goals etc.
	}
	// Indicate that all data structures has been initialized/restored.
	this._init_done = true;
	this._loaded_data = null; 
	// the loaded data has no more use now after that _init_done is true.
	this.AfterInit()
}
/*
 * This method is called by OpenTTD when an (auto)-save occurs. You should
 * return a table which can contain nested tables, arrays of integers,
 * strings and booleans. Null values can also be stored. Class instances and
 * floating point values cannot be stored by OpenTTD.
 */
function Save()
{
	Log.Info("Saving data to savegame", Log.LVL_INFO);

	// In case (auto-)save happens before we have initialized all data,
	// save the raw _loaded_data if available or an empty table.
	if (!this._init_done) {
		return this._loaded_data != null ? this._loaded_data : {};
	}

	return { 
		some_data = null,
		//some_other_data = this._some_variable,
	};
}

/*
 * When a game is loaded, OpenTTD will call this method and pass you the
 * table that you sent to OpenTTD in Save().
 */
function Load(version, tbl)
{
	Log.Info("Loading data from savegame made with version " + version + " of the game script", Log.LVL_INFO);

	// Store a copy of the table from the save game
	// but do not process the loaded data yet. Wait with that to Init
	// so that OpenTTD doesn't kick us for taking too long to load.
	this._loaded_data = {}
	foreach(key, val in tbl) {
		this._loaded_data.rawset(key, val);
	}
	this._loaded_from_version = version;
}


}




