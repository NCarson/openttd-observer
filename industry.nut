

/** Class to introspect and manage industry trading. 
	Also provides a class instance to work with OBTradeProposal.*/
class OBIndustry {

	/** Returns an array of valid industry keys */
	static function GetIndustries() {
		local a = []
		foreach (key, _ in GSIndustryList()) {
			if (!GSIndustry.IsValidIndustry(key))
				continue
			a.push(key)
		}
		return a
	}

	key = null

	constructor(key) { // we need an instance so we can normalize industry and town methods
		this.key = key
	}
	function GetTile() { return GSIndustry.GetLocation(this.key) }
	function GetName() { return GSIndustry.GetName(this.key) }

	static function ToArray() {
		local industries = []
		foreach (key in OBIndustry.GetIndustries()) {
			local industry = {}
			industry.name <- GSIndustry.GetName(key)
			local tile = GSIndustry.GetLocation(key)
			industry.x <- GSMap.GetTileX(tile)
			industry.y <- GSMap.GetTileY(tile)
			industries.append(industry)
		}
		return industries
	}

	/** Returns a table { producer_key = [trade_proposal, ...] } 
		of the possible consumers for the producer given industry keys and a cargo key.
		Table array values are instances of OBTradeProposal.
	*/
	static function GetIndustryTrades(keys, cargo) {

		local pairs = OBIndustry.GetIndustryPairs(keys, cargo)
		local trades = {}
		foreach (pair in pairs) {
			trades[pair[0]] <- [] // set up producer : [consumer, ...] table
		}
		
		foreach (pair in pairs) {
			local p = OBIndustry(pair[0])
			local c = OBIndustry(pair[1])
			trades[p.key].push(OBTradeProposal(p, c, cargo))
		}
		return trades

	}

	/** Returns an array in form of [[producer_key, consumer_key], ....] of possible trade pairs given industry keys and a cargo key.*/
	static function GetIndustryPairs(keys, cargo) 
	{
		local out = []
		foreach (producer in keys) {
			foreach (consumer in keys) {
				if (producer != consumer
					&& (Array.contains(OBIndustry.GetProducedCargos(producer), cargo))
					&& (Array.contains(OBIndustry.GetAcceptedCargos(consumer), cargo))
				) {
					out.push([producer, consumer])
				}
			}
		}
		return out
	}

	/** Returns an array of accepted cargos given an industry key.*/
	static function GetAcceptedCargos(key) 
	{
		local a = []
		foreach (ckey, _ in GSCargoList()) {
			if (GSIndustry.IsCargoAccepted(key, ckey) != GSIndustry.CAS_NOT_ACCEPTED)
				a.push(ckey)
		}
		return a
	}

	/** Returns an array of produced cargos given an industry key.*/
	static function GetProducedCargos(key) 
	{
		local a = []
		local prod
		foreach (ckey, _ in GSCargoList()) {
			prod = GSIndustry.GetLastMonthProduction(key, ckey)
			if (prod > 0)
				a.push(ckey)
		}
		return a
	}
}
