
bits = 4611686018427387904
bits = -9223372036854775808 # len 20
# 5 per asxassssssds:ine (64 * 5)

def setbit(b, n):
	return  b | 1 << n

def clearbit(b, n):
	return  b & (1 << n) ^ -1

def checkbit(b, n):
	if ((b & 1 << n) != 0):
		return True
	return False

print(checkbit(bits, 63))
