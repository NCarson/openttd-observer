
class OBBaseTradeRoute {

	devalue_rate = 0.0033 // per week

	function GetPossibleTrips() { throw "not implimented" }
	function GetTripsNeeded() { throw "not implimented" }
	function GetCapacity() { throw "not implimented" } // per trip
	function GetTripProfit() { throw "not implimented" }
	function GetRunningCost() { throw "not implimented" }
	function GetVehiclesCost() {throw "not implimented"}

	function GetServiced() {
		local serviced = this.GetPossibleTrips() / this.GetTripsNeeded()
		return serviced
	}

	function GetROI(years=1) { // pct
		local current = (
			(this.GetProfit() * years)
			+ this.DevalueVehicles(years)
			+ this.SellCapital()
			)
		local cost = this.GetCapitalCost()

		return Math.round((current - cost) / cost * 100)
	}

	function GetIncome() {
		local serviced = this.GetServiced()
		if (serviced > 1)
			serviced = 1
		return Math.round(this.GetTripProfit() * this.GetTripsNeeded() * serviced)
	}

	function GetProfit() {
		return this.GetIncome() - this.GetRunningCost()
	}

	function GetCapitalCost() {
		return this.GetVehiclesCost() + this.GetBuildCost()
	}

	function DevalueVehicles(years) {
		local capital = this.GetVehiclesCost()
		for (local i=0; i<52*years; i++) {
			capital -= (this.devalue_rate * capital)
		}
		return Math.round(capital)
	}

	function SellCapital() {
		if (this.cost_estimator == null) //FIXME make esitimator required
			return null
		return cost_estimator.SellCapital()
	}

	function GetBuildCost() {
		if (this.cost_estimator == null)
			return null
		return cost_estimator.GetCapitalCost()
	}

	function GetInfrastructureCost() {
		if (this.cost_estimator == null)
			return null
		return cost_estimator.GetInfrastructureCost()
	}
}

