
class OBTown {
	static function GetTowns() {
		local out = []
		foreach(key, _ in GSTownList()) {
			if (GSTown.IsValidTown(key))
				out.append(key)
		}
		return out
	}

	static function ToArray() {
		local towns = []
		foreach (key in OBTown.GetTowns()) {
			local town = {}
			town.name <- GSTown.GetName(key)
			town.population <- GSTown.GetPopulation(key)
			town.house_num <- GSTown.GetHouseCount(key)
			local tile = GSTown.GetLocation(key)
			town.x <- GSMap.GetTileX(tile)
			town.y <- GSMap.GetTileY(tile)
			towns.append(town)
		}
		return towns
	}
}
