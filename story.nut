

class OBStoryPage {
	
	constructor() {
		local Story = GSStoryPage
		local page = Story.New(GSCompany.COMPANY_INVALID, "Mein Kopf")
		local ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
		local el1 = Story.NewElement(page, Story.SPET_TEXT, 0, ipsum)
		local ref = Story.MakeTileButtonReference(
			Story.SPBC_DARK_BLUE, 
			Story.SPBF_NONE,
			Story.SPBC_MOUSE
			)
		local el2 = Story.NewElement(page, Story.SPET_BUTTON_TILE, ref, "an tile")
		local t = "{{GREEN}}hello{{RIGHT}}right"
		local el3 = Story.NewElement(page, Story.SPET_TEXT, 0, t)
	}
}
