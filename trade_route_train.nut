class OBTradeRouteTrain extends OBBaseTradeRoute {

	min_service_rate = 0.8

	engine = null
	wagons = null
	production = null
	cost_estimator = null
	num_trains = 1

	constructor(engine, wagons, production, cost_estimator=null) {
		this.engine = engine
		this.wagons = wagons
		this.production = production
		this.cost_estimator = cost_estimator

		while (this.GetServiced() < this.min_service_rate)
			this.num_trains += 1
	}

	function _cmp(other) {
		local t_cmp = this.GetROI(1)
		local o_cmp = other.GetROI(1)
		if (t_cmp > o_cmp) return 1
		if (t_cmp < o_cmp) return -1
		return 0
	}

	function tostring() {
		local serviced = this.GetServiced()
		local clamped_service = serviced > 1 ? 1 : serviced
		local profit = Math.round((this.GetProfit())/1000)
		local trips = Math.round(this.GetPossibleTrips() * clamped_service, 1)
		local delivered = Math.round(this.production * clamped_service)
		return (
			"$" + profit + "k yearly : ("
			+ this.num_trains + " trains) "
			+ delivered + " units delivered, "
			+ trips + " trips, "
			+ Math.round(serviced*100) + "% serviced"
			)
	}

	function GetPossibleTrips() {
		return (365 / this.wagons.GetDaysPerTrip()) * this.num_trains
	}

	function GetTripsNeeded() {
		return this.production / this.GetCapacity()
	}

	function GetCapacity() { // per trip
	 	return this.wagons.GetCapacity() + this.engine.GetCapacity()
	}

	function GetTripProfit() {
		return this.wagons.GetIncomePerUnit() * this.GetCapacity()
	}

	function GetRunningCost() {
		return (
			(this.engine.GetRunningCost() + this.wagons.GetRunningCost()) 
			* this.num_trains
			)
	}

	function GetVehiclesCost() {
		return (
			(this.engine.GetVehicleCost() + this.wagons.GetVehicleCost()) 
			* this.num_trains
			)
	}

}

class OBTradeRouteEngine {

	key = null
	cargo = null
	distance = null // tiles

	length = 0.5 //FIXME

	constructor(key, cargo, distance, train_length) { //FIXME get rid of train_length

		this.key = key
		this.cargo = cargo
		this.distance = distance
	}

	function tostring() {
		local name = GSEngine.GetName(this.key)
		local mph = OBUtil.GetMph(this.GetMaxSpeed())
		local hp = this.GetPower()
		return (name + " : " + mph + " mph, " + hp + " hp")
	}

	function IterWagons(engines, train_length) {
		foreach (key in OBEngine.IterCanHaul(engines, this.cargo))
			yield OBTradeRouteWagons(key, this, train_length)
	}

	function GetPower() {
		return GSEngine.GetPower(this.key)
	}

	function GetWeight() { // metric tons
		return GSEngine.GetWeight(this.key)
	}

	function GetCapacity() {
		return GSEngine.GetCapacity(this.key)
	}

	function GetRunningCost() {
		return GSEngine.GetRunningCost(this.key)
	}

	function GetVehicleCost() {
		return GSEngine.GetPrice(this.key)
	}

	function GetMaxSpeed() {
		return GSEngine.GetMaxSpeed(this.key)
	}
}

class OBTradeRouteWagons { // more like an engine with wagons

	efficiency = 0.8
	length = 0.5 //FIXME

	key = null
	engine = null
	wagon_num = null

	constructor(key, engine, train_length) {
		this.key = key
		this.engine = engine
		this.wagon_num = (train_length - this.engine.length) / this.length
	}

	function tostring() {

		local days = this.GetDaysPerTrip()
		local speed = this.GetMaxSpeed()
		local cargo = GSCargo.GetName(this.engine.cargo)
		local income = GSCargo.GetCargoIncome(
			this.engine.cargo, 
			this.engine.distance, 
			Math.round(days)
			)
		local ratio = Math.round(this.engine.GetPower() / this.GetWeight(), 1)

		return (
			GSEngine.GetName(this.key) + " X " 
			+ this.wagon_num + " : " 
			+ this.GetCapacity() + " " + cargo
			//+ ", " + this.GetWeight() + "t"
			+ ", " + Math.round(days)
			+ " days @ " + OBUtil.GetMph(speed) + " mph"
			+ " -> $" + income + " per unit"
			//+ ", " + ratio + " hp/t"
			)
	}

	function GetWeight() { // metric tons

		local wagons = GSEngine.GetWeight(this.key) * this.wagon_num
		local engine = this.engine.GetWeight()
		local cargo = GSCargo.GetWeight(this.engine.cargo, this.GetCapacity())
		return wagons + engine + cargo
	}

	function GetIncomePerUnit() {

		return GSCargo.GetCargoIncome(
			this.engine.cargo, 
			this.engine.distance, 
			Math.round(this.GetDaysPerTrip()))
	}

	function GetDaysPerTrip() {
		local loading = 20 //FIXME figure out load/unload times
		return this.engine.distance / this.GetTilesPerDay() * 2 + loading
	}

	function GetTilesPerDay() {

		local speed = this.GetMaxSpeed()
		return OBUtil.GetTilesPerDay(speed * this.efficiency)
	}

	function GetMaxSpeed() {
		local speed = GSEngine.GetMaxSpeed(this.key)
		local espeed = this.engine.GetMaxSpeed()
		if (!speed) // no speed limit
			return espeed
		else if (speed < espeed) // wagon is limiter
			return speed
		else
			return espeed
	}

	function GetVehicleCost() {
		return GSEngine.GetPrice(this.key) * this.wagon_num
	}

	function GetRunningCost() {
		return GSEngine.GetRunningCost(this.key) * this.wagon_num
	}

	function GetCapacity() {
		return (GSEngine.GetCapacity(this.key) * this.wagon_num).tointeger()
	}
}

